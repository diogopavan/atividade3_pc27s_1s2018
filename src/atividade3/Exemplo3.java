/**
 * Exemplo1: Programacao com threads
 * Autor: Diogo Reis Pavan
 * Ultima modificacao: 28/03/2018
 */
package atividade3;

import java.util.Random;

/**
 *
 * @author Diogo Reis Pavan
 */
public class Exemplo3 {

    public static void main(String[] args) {

        System.out.println("Inicio da criacao das threads.");

        //Cria cada thread com um novo runnable selecionado
        Thread t1 = new Thread(new PrintTasks("thread1", 1));
        Thread t2 = new Thread(new PrintTasks("thread2", 2));
        Thread t3 = new Thread(new PrintTasks("thread3", 3));

        //Inicia as threads, e as coloca no estado EXECUTAVEL
        t1.start(); //invoca o método run de t1
        t2.start();
        t3.start();

        System.out.println("Threads criadas");
    }

}
