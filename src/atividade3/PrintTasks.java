/**
 * Exemplo1: Programacao com threads
 * Autor: Diogo Reis Pavan
 * Ultima modificacao: 28/03/2018
 */
package atividade3;

import java.time.LocalDateTime;

/**
 *
 * @author Diogo Reis Pavan
 */
public class PrintTasks implements Runnable {

    private final String taskName; //nome da tarefa
    private int cor;

    public PrintTasks(String name, int cor) {
        taskName = name;
        this.cor = cor;
    }

    public void run() {
        try {
            while (true) {
                switch (cor) {
                    case 1:
                        System.out.println(taskName + " - Cor: Verde, Horário: " + LocalDateTime.now());
                        cor++;
                        break;
                    case 2:
                        System.out.println(taskName + " - Cor: Amarela, Horário: " + LocalDateTime.now());
                        cor++;
                        break;
                    case 3:
                        System.out.println(taskName + " - Cor: Vermelho, Horário: " + LocalDateTime.now());
                        cor = 1;
                        break;
                    default:
                        break;
                }
            }
        } catch (Exception ex) {
            System.out.printf("%s %s\n", taskName, "terminou de maneira inesperada.");
        }
        System.out.printf("%s acordou!\n", taskName);
    }

}
